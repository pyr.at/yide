module.exports = {
  root: true,
  parser: 'babel-eslint',
  parserOptions: {
    sourceType: 'module'
  },
  env: {
    browser: true,
    node: true,
  },
  extends: 'standard',
  globals: {
    __static: true,
  },
  plugins: [
    'html'
  ],
  'rules': {
    // Allow paren-less arrow functions
    'arrow-parens': 0,
    // Allow async-await
    'generator-star-spacing': 0,
    // Allow debugger during development
    'no-debugger': process.env.NODE_ENV === 'production' ? 2 : 0,

    // ---------------------------------------------------------- //

    // Sometimes useful during development (no error, just warn)
    'no-useless-return': 'warn',

    // ---------------------------------------------------------- //

    'no-useless-escape': 'off', // conflicts with some XRegExp escapes
    'no-control-regex':  'off', // somewhat necessary when filtering/disallowing exactly those
    // 'no-mixed-operators': "off", // move responsibility (for readability) to the developer
    // Allow missing curly braces for single statements (personal preference / readability)
    'curly': ['off', 'multi-or-nest'], // multi-or-nest could be nice, but makes development tedious

    // Don't require spaces around infix operators (pp / readability depends on use case + use of spaces)
    'space-infix-ops': ['off', /*{ 'int32Hint': true }*/],

    // No space after colons in single line listings (pp / readability)
    'key-spacing': ['error', {
      'singleLine': { 'beforeColon':false, 'afterColon':false },
      'multiLine': {
          'beforeColon' :false,
          'afterColon': true,
          'align':      'value'
      }
    }],
 
    // Require dangling commas on multiline (barely a pp, this is a must, sorry IE8)
    // ('single line' and 'single element' would be up for discussion, if available)
    'comma-dangle': ['error', 'always-multiline'],
  }
}
