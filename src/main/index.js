'use strict'

import { app, BrowserWindow, Menu, ipcMain, dialog } from 'electron'
// const { ipcMain } = require('electron')

/**
 * Set `__static` path to static files in production
 * https://simulatedgreg.gitbooks.io/electron-vue/content/en/using-static-assets.html
 */
if (process.env.NODE_ENV !== 'development') // {
  global.__static = require('path').join(__dirname, '/static').replace(/\\/g, '\\\\')
// }

let mainWindow
const winURL = process.env.NODE_ENV === 'development'
  ? `http://localhost:9080`
  : `file://${__dirname}/index.html`

// ---------------------------------------------------------------------------------------------- //

function createWindow () {
  /**
   * Initial window options
   */
  mainWindow = new BrowserWindow({
    height:         800,
    useContentSize: true,
    width:          1200,
    webPreferences: {
      nodeIntegration:         true,
      nodeIntegrationInWorker: true,
    },
  })

  mainWindow.loadURL(winURL)
  mainWindow.setTitle('Loading yIDE ...') // not really necessary here, "loading title"

  mainWindow.on('closed', () => {
    mainWindow = null
  })

  Menu.setApplicationMenu(Menu.buildFromTemplate([
    {
      label:   '&File',
      submenu: [
        {
          label:       `&New`,
          accelerator: 'CmdOrCtrl+N',
          click:       () => { mainWindow.webContents.send('request', ['new']) },
        },
        {
          label:       `&Open`,
          accelerator: 'CmdOrCtrl+O',
          click:       () => { mainWindow.webContents.send('request', ['open']) },
          // NOTE: Always run by renderer, even if seemingly not necessary (veto?)
        },
        {
          label:       `&Save`,
          accelerator: 'CmdOrCtrl+S',
          click:       () => { mainWindow.webContents.send('request', ['save']) },
        },
        {type:'separator'},
        {
          label:       `Quit`,
          accelerator: 'CmdOrCtrl+W',
          click:       () => app.quit(),
        },
      ],
    },
  ]))
}

// ---------------------------------------------------------------------------------------------- //

app.on('ready', createWindow)
app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') // {
    app.quit()
  // }
})
app.on('activate', () => {
  if (mainWindow === null) // {
    createWindow()
  // }
})

// ---------------------------------------------------------------------------------------------- //

const fileFilter = [
  {name:'y Files', extensions:['y']},
  {name:'All Files', extensions:['*']},
]
ipcMain.on('command', (event, data) => {
  if (data.length < 2) console.warn('IPC command with less than two arguments :-/')
  else switch (data[0]) {
    // Open
    case 'open':
      const openPath = dialog.showOpenDialogSync({filters:fileFilter})
      if (openPath && openPath[0]) { // could be multiple files, with other flag
        // console.log('TODO: Open path', openPath[0])
        try {
          const rawdatautf8 = require('fs').readFileSync(openPath[0], 'utf8')
          const composition = require('js-yaml').safeLoad(rawdatautf8)
          // const data = JSON.parse(require('fs').readFileSync(openPath[0]))
          // TODO: Regex to check for correct file (also before/after saving!)
          console.log('opened composition', composition)
          if (composition && composition[0])
            event.sender.send('request', ['opened', composition])
        } catch (e) { console.log('un problemo opening compositiono', e) }
        //   dialog.showMessageBox({title:'Success', message:'File opened successfully.'}) // only after renderer conformation
      }
      break

    // Save
    case 'save': // console.log(data)
      const path = dialog.showSaveDialogSync({filters:fileFilter})
      if (path) {
        const yamlified = require('js-yaml').safeDump(data[1])
        require('fs').writeFileSync(path, yamlified) // JSON.stringify(data[1]))
        console.log('Written to file', path, yamlified)
      }
      break
    // case 'download':
    //   require('request')(item.getUrl(), function(data) {
    //     require('fs').writeFileSync('/somewhere', data);
    //   });
  }
})

// ---------------------------------------------------------------------------------------------- //

// For testing purposes onl
ipcMain.on('ping', (event, data) => {
  console.log('ping received')
  // ipcMain.emit('pong', Math.random())
  event.sender.send('pong', Math.random())
})

/**
 * Auto Updater
 *
 * Uncomment the following code below and install `electron-updater` to
 * support auto updating. Code Signing with a valid certificate is required.
 * https://simulatedgreg.gitbooks.io/electron-vue/content/en/using-electron-builder.html#auto-updating
 */

/*
import { autoUpdater } from 'electron-updater'

autoUpdater.on('update-downloaded', () => {
  autoUpdater.quitAndInstall()
})

app.on('ready', () => {
  if (process.env.NODE_ENV === 'production') autoUpdater.checkForUpdates()
})
 */
